
#ifndef SteppingActionManager_h
#define SteppingActionManager_h 1

#include "G4UserSteppingAction.hh"

#include "globals.hh"

#include <map>
#include <string>

/// Stepping action class

class G4Timer;

class SteppingActionManager : public G4UserSteppingAction
{
  public:
    SteppingActionManager();
    virtual ~SteppingActionManager();

    virtual void    UserSteppingAction(const G4Step* );
	
	void SetSteppingAction(std::string,G4UserSteppingAction* );
  private:
  	
	int nInstances=0;
	
	std::map<std::string,G4UserSteppingAction*> steppingActionList;
};

#endif
