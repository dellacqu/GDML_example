#ifndef SIMG4COMMON_SingleParticleGeneratorAction_H
#define SIMG4COMMON_SingleParticleGeneratorAction_H

#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4ParticleGun.hh"
#include "G4ThreeVector.hh"
#include "G4Event.hh"
#include "G4VPrimaryGenerator.hh"

class G4ParticleGun;
class SingleParticleGeneratorMessenger;

class SingleParticleGeneratorAction: public G4VUserPrimaryGeneratorAction {
public:
	SingleParticleGeneratorAction();
	~SingleParticleGeneratorAction() {;}
	
	void GeneratePrimaries(G4Event* anEvent);
	
	void SetParticleType(std::string particleName);
	void SetEnergyRange(double eMin,double eMax);
	void SetEtaRange(double etaMin,double etaMax);
	void SetPhiRange(double phiMin=0,double phiMax=2*M_PI);
	void SetVertexPosition(double vX,double vY,double vZ);
	void SetEnergyMin(double e) {eMin=e;}
	void SetEnergyMax(double e) {eMax=e;}
	void SetEtaMin(double e) {etaMin=e;}
	void SetEtaMax(double e) {etaMax=e;}
	void SetPhiMin(double p) {phiMin=p;}
	void SetPhiMax(double p) {phiMax=p;}
	
	std::string GetParticleType() {return particle;}
	double GetEnergyMin() {return eMin;}
	double GetEnergyMax() {return eMax;}
	double GetEtaMin() {return etaMin;}
	double GetEtaMax() {return etaMax;}
	double GetPhiMin() {return phiMin;}
	double GetPhiMax() {return phiMax;}
	
	static double GetEtaValue() {return currentEta;}
	static double GetPhiValue() {return currentPhi;}
	
private:
	double eMin,eMax;
	double etaMin,etaMax;
	double phiMin,phiMax;
	std::string particle;
	
	double vertX,vertY,vertZ;
	
	static double currentEta;
	static double currentPhi;
	
	SingleParticleGeneratorMessenger* theMessenger;
};

#endif
