 
#ifndef RadLengthIntegrator_h
#define RadLengthIntegrator_h 1

#include "G4UserSteppingAction.hh"
#include "G4UserEventAction.hh"

#include "globals.hh"

#include <map>
#include <string>

/// Stepping action class

class G4Timer;

class RadLengthIntegrator : public G4UserSteppingAction, public G4UserEventAction
{
  public:
    RadLengthIntegrator();
    virtual ~RadLengthIntegrator();

    virtual void    UserSteppingAction(const G4Step* );
	
	virtual void    BeginOfEventAction(const G4Event* );
    virtual void    EndOfEventAction(const G4Event* );
	
  private:
  	
	std::map<std::string,double> radLengths;
};

#endif
