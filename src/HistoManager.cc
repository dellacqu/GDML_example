
#include <TH1D.h>
#include <TH2D.h>
#include <TProfile.h>
#include <TFile.h>
#include <TTree.h>
#include <CLHEP/Units/SystemOfUnits.h>

#include "HistoManager.hh"
#include "G4UnitsTable.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

HistoManager::HistoManager()
:fRootFile(0),fileName("")
{
}

HistoManager::HistoManager(std::string s)
:fRootFile(0),fileName(s)
{
}

HistoManager* HistoManager::GetHistoManager(std::string name)
{
	static HistoManager* hManager=0;
	if (!hManager) hManager=new HistoManager(name);
	return hManager;
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

HistoManager::~HistoManager()
{
  if (fRootFile) delete fRootFile;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void HistoManager::Book()
{ 
  // Creating a tree container to handle histograms and ntuples.
  // This tree is associated to an output file.
  //
  fRootFile = new TFile(fileName.c_str(),"RECREATE");
  if (! fRootFile) {
    G4cout << " HistoManager::Book :" 
           << " problem creating the ROOT TFile "
           << G4endl;
    return;
  }
  

 
  G4cout << "\n----> Output file is open in " << fileName << G4endl;
}

TH1D* HistoManager::Book1DHistogram(std::string name, std::string type, int nChX, double minX, double maxX)
{
	if (histogramMap.find(name)!=histogramMap.end())
		return dynamic_cast<TH1D*>(histogramMap[name]);
	else 
	{
		TH1D* h=new TH1D(name.c_str(),type.c_str(),nChX,minX,maxX);
		histogramMap[name]=h;
		return h;
	}
}
void HistoManager::BookAndFill1D(std::string name, std::string type, int nChX, double minX, double maxX, double xval, double w)
{
	TH1D* h=Book1DHistogram(name.c_str(),type.c_str(),nChX,minX,maxX);
	if (h) h->Fill(xval,w);
	else
	{
		std::cout <<" Something is wrong with histogram "<<name<<" "<<type<<std::endl;
	}
}
TProfile* HistoManager::BookProfileHistogram(std::string name, std::string type, int nChX, double minX, double maxX, double minY, double maxY)
{
	if (histogramMap.find(name)!=histogramMap.end())
		return dynamic_cast<TProfile*>(histogramMap[name]);
	else
	{
		TProfile* h=new TProfile(name.c_str(),type.c_str(),nChX,minX,maxX,minY,maxY);
		histogramMap[name]=h;
		return h;
	}
}
void HistoManager::BookAndFillProfile(std::string name, std::string type, int nChX, double minX, double maxX, double minY, double maxY, double xval, double yval, double w )
{
 	TProfile *h=BookProfileHistogram(name.c_str(),type.c_str(),nChX,minX,maxX,minY,maxY);
	if (h) h->Fill(xval,yval);
	else
	{
		std::cout <<" Something is wrong with histogram "<<name<<" "<<type<<std::endl;
	}
}
TH2D* HistoManager::Book2DHistogram(std::string name, std::string type, int nChX, double minX, double maxX,int nChY, double minY, double maxY)
{
	if (histogramMap.find(name)!=histogramMap.end())
		return dynamic_cast<TH2D*>(histogramMap[name]);
	else
	{
		TH2D* h=new TH2D(name.c_str(),type.c_str(),nChX,minX,maxX,nChY,minY,maxY);
		histogramMap[name]=h;
		return h;
	}
} 
void HistoManager::BookAndFill2D(std::string name, std::string type, int nChX, double minX, double maxX,int nChY, double minY, double maxY, double xval, double yval, double w)
{
 	TH2D *h=Book2DHistogram(name.c_str(),type.c_str(),nChX,minX,maxX,nChY,minY,maxY);
	if (h) h->Fill(xval,yval,w);
	else
	{
		std::cout <<" Something is wrong with histogram "<<name<<" "<<type<<std::endl;
	}
} 

void HistoManager::Save()
{ 
  if (! fRootFile) return;
  
  fRootFile->Write();       // Writing the histograms to the file
  fRootFile->Close();       // and closing the tree (and the file)
  
  G4cout << "\n----> Histograms and ntuples are saved\n" << G4endl;
}



