#include "ActionInitialization.hh"
#include "RunActionManager.hh"
#include "RunAction.hh"
#include "EventActionManager.hh"
#include "SteppingActionManager.hh"
#include "SingleParticleGeneratorAction.hh"
#include "RadLengthIntegrator.hh"

ActionInitialization::ActionInitialization()
 : G4VUserActionInitialization()
{}
ActionInitialization::~ActionInitialization()
{}
void ActionInitialization::BuildForMaster() const
{
  SetUserAction(new RunActionManager);
}
void ActionInitialization::Build() const
{
  SingleParticleGeneratorAction* pGenerator=new SingleParticleGeneratorAction;

  SetUserAction(pGenerator);
  
  RadLengthIntegrator* radLen=new RadLengthIntegrator;
  
  RunActionManager* runAction=new RunActionManager;
  SetUserAction(runAction);
  
  EventActionManager* eventAction=new EventActionManager;
  eventAction->SetEventAction("RadLengthIntegrator",radLen);
  SetUserAction(eventAction);
  
  SteppingActionManager* steppingAction=new SteppingActionManager;
  steppingAction->SetSteppingAction("RadLengthIntegrator",radLen);
  SetUserAction(steppingAction);
}
