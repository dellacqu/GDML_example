
#include "EventActionManager.hh"

#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4TrajectoryContainer.hh"
#include "G4Trajectory.hh"
#include "G4ios.hh"
#include "G4Timer.hh"

#include <iostream>

EventActionManager::EventActionManager()
: G4UserEventAction()
{
	std::cout<<" EventActionManager being created "<<std::endl;
}


EventActionManager::~EventActionManager()
{
	//for (auto it : eventActionList)
	//	delete it.second;
}

void EventActionManager::BeginOfEventAction(const G4Event* ev)
{
	if (nInstances)
	for (auto it : eventActionList)
		it.second->BeginOfEventAction(ev);
}

void EventActionManager::EndOfEventAction(const G4Event* ev)
{
	if (nInstances)
	for (auto it : eventActionList)
		it.second->EndOfEventAction(ev);
}  

void EventActionManager::SetEventAction(std::string name,G4UserEventAction* ev)
{
	if (eventActionList.find(name)!=eventActionList.end())
	{
		std::cout<<" EventActionManager error! action "<<name<<" being redefined!"<<std::endl;
	}
	else
	{
	    nInstances++;
		eventActionList[name]=ev;
	}
}
