
#include "SteppingActionManager.hh"

#include "G4ios.hh"
#include "G4Timer.hh"

#include <iostream>

SteppingActionManager::SteppingActionManager()
: G4UserSteppingAction()
{
	std::cout<<" SteppingActionManager being created "<<std::endl;
}


SteppingActionManager::~SteppingActionManager()
{
	//for (auto it : steppingActionList)
	//	delete it.second;
}

void SteppingActionManager::UserSteppingAction(const G4Step* step)
{
	if (nInstances)
	for (auto it : steppingActionList)
		it.second->UserSteppingAction(step);
} 

void SteppingActionManager::SetSteppingAction(std::string name,G4UserSteppingAction* step)
{
	if (steppingActionList.find(name)!=steppingActionList.end())
	{
		std::cout<<" SteppingActionManager error! action "<<name<<" being redefined!"<<std::endl;
	}
	else
	{
		nInstances++;
		steppingActionList[name]=step;
	}
}
