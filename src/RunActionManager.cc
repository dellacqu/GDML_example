
#include "RunActionManager.hh"

#include "G4ios.hh"
#include "G4Timer.hh"

#include <iostream>

RunActionManager::RunActionManager()
: G4UserRunAction()
{
	std::cout<<" RunActionManager being created "<<std::endl;
}


RunActionManager::~RunActionManager()
{
	//for (auto it : runActionList)
	//	delete it.second;
}

void RunActionManager::BeginOfRunAction(const G4Run* run)
{
    std::cout<<"Begin of run"<<std::endl;
	if (nInstances)
	for (auto it : runActionList)
		it.second->BeginOfRunAction(run);
}

void RunActionManager::EndOfRunAction(const G4Run* run)
{
	if (nInstances)
	for (auto it : runActionList)
		it.second->EndOfRunAction(run);
	std::cout<<" End of Run "<<std::endl;
}  

void RunActionManager::SetRunAction(std::string name,G4UserRunAction* run)
{
    nInstances++;

	if (runActionList.find(name)!=runActionList.end())
	{
		std::cout<<" RunActionManager error! action "<<name<<" being redefined!"<<std::endl;
	}
	else
	{
		runActionList[name]=run;
	}
}
